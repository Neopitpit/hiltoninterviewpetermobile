import React from 'react';
import {
  StatusBar,
} from 'react-native';
import { ApolloProvider } from 'react-apollo';
import { createAppContainer } from 'react-navigation';
import createApolloClient from './apollo';
import TabNavigator from './src/routes/mainRoutes';

const AppContainer = createAppContainer(TabNavigator);
const client = createApolloClient();

export default class App extends React.Component {
  
  render() {
    return (
      <ApolloProvider client={client}>
        <StatusBar barStyle="light-content" />
        <AppContainer />
      </ApolloProvider>
    );
  }
};
