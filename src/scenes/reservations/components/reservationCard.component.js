import React from 'react';
import {
  View,
  Text,
  Dimensions,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import Helpers from '@hilton-app/helpers/helpers';
import { DATE_FORMAT } from '@hilton-app/global/constants';
import theme from '@hilton-app/global/theme';
import HotelMainImageSmall from '@hilton-app/components/hotelMainImageSmall.component';

class ReservationCard extends React.Component {
    static propTypes = {
        hotelName: PropTypes.string,
        arrivalDate: PropTypes.string,
        departureDate: PropTypes.string,
        price: PropTypes.number,
        imageURL: PropTypes.string,
    };
    
    static defaultProps = {
        hotelName: '',
        arrivalDate: null,
        departureDate: null,
        price: 0,
        imageURL: null,
    };

    render() {
        const {
            hotelName,
            arrivalDate,
            departureDate,
            price,
            imageURL
        } = this.props;
        const { width } = Dimensions.get('window');

        const arrivalDateMoment = moment(arrivalDate);
        const departureDateMoment = moment(departureDate);
        const arrivalDateText = arrivalDateMoment.format(DATE_FORMAT);
        const departureDateText = departureDateMoment.format(DATE_FORMAT);
        const numberNights = Helpers.numberOfNights(arrivalDateMoment, departureDateMoment);

      return (
        <View style={[styles.container, { width: width, }]}>
            <HotelMainImageSmall imageURL={imageURL} />
            <View style={styles.detailsContainer}>
                <Text style={styles.hotelName}
                    ellipsizeMode='tail'
                    numberOfLines={1}
                >{hotelName}</Text>
                <View style={styles.containerDate}>
                    <Text style={styles.dateText}>{arrivalDateText} - {departureDateText} 
                    <Text style={styles.dateTextNight}>- {numberNights} night(s)</Text>
                    </Text>
                </View>
                <Text style={styles.priceContainer}>
                    <Text style={styles.priceText}>{price} $ US</Text>
                </Text>
            </View>
        </View>
      );
    }
}

export default ReservationCard;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        height: 110,
        backgroundColor: theme.backgroundColorPrimary,
        marginTop:5,
        marginBottom:5,
    },
    hotelName: {
        fontSize: 16, 
        fontWeight:'bold', 
        color: theme.inverseTextColor,
    },
    detailsContainer: {
        flex: 1,
        flexDirection: 'column',
        padding:10,
        backgroundColor: '#FFF'
    },
    containerDate: {
        flexDirection: 'row',
        paddingTop: 5,
    },
    dateText: {
        fontSize: 12,
        fontWeight:'bold',
        color: theme.inverseTextColor,
    },
    dateTextNight: {
        fontSize: 12,
        fontWeight:'400',
        color: theme.inverseTextColor,
    },
    priceContainer: {
        paddingTop: 5,
    },
    priceText: {
        fontSize: 12,
        fontWeight:'bold',
        color: 'black',
    },
  });
