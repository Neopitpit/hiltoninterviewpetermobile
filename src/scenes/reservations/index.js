import React from 'react';
import {
  SafeAreaView,
  FlatList,
  View,
  Text,
  StyleSheet,
} from 'react-native';
import { withNavigationFocus } from 'react-navigation';
import { graphql } from 'react-apollo';

import * as ReservationQuery from '@hilton-app/queries/reservation.graphql';
import ReservationCard from './components/reservationCard.component';
import { URI } from '@hilton-app/global/constants';
import themeStyle from '@hilton-app/global/theme.styles';
import theme from '@hilton-app/global/theme';

class Reservations extends React.Component {

  componentDidFocus = () => {
    console.log("FOCUS");
    this.props.reservations.refetch();
  }

  componentDidMount(netProps) {
    this.subs = [
      this.props.navigation.addListener('didFocus', (payload) => this.componentDidFocus()),
    ];
  }

  renderItem = (row) => {
    const { item } = row;
    const {
      _id,
      hotel,
      arrivalDate,
      departureDate,
      price,
    } = item;

    const { name, imageMain } = hotel;
    const imageURL = URI + imageMain.url;

    return (
      <ReservationCard
        key={_id}
        hotelName={name}
        arrivalDate={arrivalDate}
        departureDate={departureDate}
        price={price}
        imageURL={imageURL}
      />
    );
  }

  renderLoading = () => {
    return (
      <View>
        <Text>Loading</Text>
      </View>
    );
  }

  render() {
    const { reservations } = this.props;

    return (
      <SafeAreaView style={themeStyle.safeAreaView}>
        <FlatList
          refreshing={reservations.loading}
          onRefresh={() => this.renderLoading}
          data={reservations.reservations}
          renderItem={this.renderItem}
          style={styles.list}
          keyExtractor={item => item._id}
          extraData={reservations.reservations}
        /> 
      </SafeAreaView>
    );
  }
}

const reservationsList = graphql(ReservationQuery.reservations, {
  name: 'reservations',
});

export default reservationsList(withNavigationFocus(Reservations, 'Reservations'));

const styles = StyleSheet.create({
  list: { 
    flex: 1,
    backgroundColor: theme.backgroundColorSecondary,
  },
});