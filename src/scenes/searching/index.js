import React from 'react';
import {
  SafeAreaView,
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import { Ionicons } from '@expo/vector-icons';
import HiltonButton from '@hilton-app/components/button.component';
import Helpers from '@hilton-app/helpers/helpers';
import { DATE_FORMAT } from '@hilton-app/global/constants';
import DateRangePickerModal from '@hilton-app/components/dateRangePicker.component';
import theme from '@hilton-app/global/theme';
const moment = require('moment');

class Searching extends React.Component {
  state = {
    modalDatePickerVisible: false,
    startDate: moment(),
    untilDate: moment().add(1, 'day'),
  };

  toggleModalDatePicker = () => {
    const { modalDatePickerVisible } = this.state;
    this.setState({modalDatePickerVisible: !modalDatePickerVisible});
  }

  onDatePickerSelected = (startDate, untilDate) => {
    this.setState({ startDate, untilDate, modalDatePickerVisible: false });
  }

  onSearch = () => {
    const { navigation } = this.props;
    const { startDate, untilDate} = this.state;
    navigation.navigate('Hotels', { startDate: startDate, untilDate: untilDate });
  }

  render() {
    const { startDate, untilDate } = this.state;
    const numberNight = Helpers.numberOfNights(startDate, untilDate);
    const dateRangeText = startDate.format(DATE_FORMAT) + ' - ' + untilDate.format(DATE_FORMAT) + ' ('+ numberNight + ' night(s))'; 

    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.searchContainer}>
          <TouchableOpacity 
            style={styles.dateRangeButton}
            onPress={this.toggleModalDatePicker}>
            <Ionicons name="md-calendar" size={24} style={styles.calendarIcon}/>
            <Text>{dateRangeText}</Text>
          </TouchableOpacity>
          <HiltonButton text={'Search'} onPress={this.onSearch} />
        </View>
        <DateRangePickerModal 
          startDate={startDate}
          untilDate={untilDate}
          isVisible={this.state.modalDatePickerVisible}
          onDatePickerSelected={this.onDatePickerSelected}
        />
      </SafeAreaView>
    );
  }
}

export default Searching;

const styles = StyleSheet.create({
  searchContainer: { 
    backgroundColor: '#FFF'
  },
  calendarIcon: {
    paddingRight: 10,
  },
  dateRangeButton: {
    flexDirection: 'row',
    backgroundColor: theme.backgroundColorSecondary,
    margin: 15,
    padding: 15,
    alignItems: 'center',
  },
});
