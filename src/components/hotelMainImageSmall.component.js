import React from 'react';
import {
  Image,
} from 'react-native';
import PropTypes from 'prop-types';

class HotelMainImageSmall extends React.Component {
  static propTypes = {
    imageURL: PropTypes.string,
  };

  static defaultProps = {
    imageURL: null,
  };

  render() {
    const { imageURL } = this.props;
    return (
          <Image source={{uri: imageURL}} style={{width: 120, height: 110}} resizeMode='stretch' />
    );
  }
}

export default HotelMainImageSmall;
