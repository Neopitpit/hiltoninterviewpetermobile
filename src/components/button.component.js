import React from 'react';
import {
  TouchableOpacity,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';
import themeStyles from '@hilton-app/global/theme.styles';

class HiltonButton extends React.Component {
  static propTypes = {
    text: PropTypes.string,
    onPress: PropTypes.func,
  };

  static defaultProps = {
    text: '',
    onPress: null,
  };

  render() {
    const { text, onPress } = this.props;

    return (
      <TouchableOpacity onPress={onPress} style={themeStyles.buttonPrimary}>
          <Text style={themeStyles.buttonPrimaryText}>{text}</Text>
      </TouchableOpacity>

    );
  }
}

export default HiltonButton;
