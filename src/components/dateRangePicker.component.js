import React from 'react';
import {
  View,
  Modal,
  Alert,
  StyleSheet,
} from 'react-native';
import DatepickerRange from 'react-native-range-datepicker';
import PropTypes from 'prop-types';
import theme from '@hilton-app/global/theme';
const moment = require('moment');

class Searching extends React.Component {
  static propTypes = {
    isVisible: PropTypes.bool,
    startDate: PropTypes.object,
    untilDate: PropTypes.object,
    onDatePickerSelected: PropTypes.func,
  };

  static defaultProps = {
    text: false,
    startDate: moment(),
    untilDate: moment().add(1, 'day'),
    onDatePickerSelected: null,
  };

  render() {
    const { isVisible, startDate, untilDate, onDatePickerSelected } = this.props;

    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={isVisible}
        onRequestClose={() => {
          Alert.alert('Modal has been closed.');
        }}>
        <View style={styles.container}>
          <View>
            <DatepickerRange
              startDate={startDate.format()}
              untilDate={untilDate.format()}
              onConfirm={( startDate, untilDate ) => onDatePickerSelected(startDate, untilDate)}
              showReset={false}
              showClose={false}
              selectedBackgroundColor={theme.brandPrimary}
              buttonColor={'#FFF'}
              buttonContainerStyle={styles.datePickerButton}
            />
          </View>
        </View>
      </Modal>
    );
  }
}

export default Searching;

const styles = StyleSheet.create({
  container: { 
    backgroundColor: '#FFF',
    marginTop: 100, 
    marginBottom: 100,
  },
  calendarIcon: {
    paddingRight: 10,
  },
  datePickerButton: {
    backgroundColor: theme.buttonPrimary, 
    borderRadius:15, 
    margin:20,
  },
});
