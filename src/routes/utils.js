import theme from '@hilton-app/global/theme';

export const headerDefault = {
    headerStyle: {
        backgroundColor: theme.navPrimary,
    },
    headerTintColor: theme.navTintColor,
    headerTitleStyle: {
    fontWeight: 'bold',
    },
};