import React from 'react'
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { Ionicons } from '@expo/vector-icons';
import { headerDefault }  from '@hilton-app/routes/utils';
import Searching from '@hilton-app/scenes/searching';
import Congratulations from '@hilton-app/scenes/congratulations';
import Hotels from '@hilton-app/scenes/hotels';
import Reservations from '@hilton-app/scenes/reservations';

const HotelsStack = createStackNavigator({
    Searching: {
      screen: Searching,
      navigationOptions: {
        headerTitle: 'Hilton',
        ...headerDefault,
      },
    },
    Hotels: {
      screen: Hotels,
      navigationOptions: {
        headerTitle: 'Hotel results',
        ...headerDefault,
      },
    },  
    Congratulations: {
      screen: Congratulations,
      navigationOptions: {
        headerTitle: 'Congratulation',
        ...headerDefault,
      },
    },  
  }, {
    navigationOptions: {
      initialRouteName: 'Searching',
      tabBarIcon: ({ tintColor }) => (
        <Ionicons name="md-calendar" size={24} color={tintColor}/>
        ),
      tabBarLabel: 'Search'
    },
  }
);

const ReservationStack = createStackNavigator({
  Reservations: {
      screen: Reservations,
      navigationOptions: {
        headerTitle: 'Your reservations',
        ...headerDefault,
      },
    },   
  }, {
    navigationOptions: {
      initialRouteName: 'Reservations',
      tabBarIcon: ({ tintColor }) => (
        <Ionicons name="md-list-box" size={24} color={tintColor}/>
        ),
      tabBarLabel: 'Reservations'
    },
  }
);

export default TabNavigator = createBottomTabNavigator({
  Hotels: HotelsStack,
  Reservation: ReservationStack,
  },
);
