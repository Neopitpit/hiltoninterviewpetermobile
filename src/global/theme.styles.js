import {
  StyleSheet,
} from 'react-native';
import theme from '@hilton-app/global/theme';

export default StyleSheet.create({
  safeAreaView: {
    flex: 1,
  },

  //BUTTONS
  buttonPrimary: {
    flexDirection: 'column',
    margin: 15,
    backgroundColor: theme.buttonPrimary,
    borderRadius: 15,
    height: 50,
    width: theme.width - 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonPrimaryText: {
    color: theme.buttonPrimaryTextColor,
    fontSize: theme.buttonPrimaryTextFontSize,
  }
});
